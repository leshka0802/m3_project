from django.conf.urls import url
from objectpack import desktop

from app import actions
from app.controller import controller


def register_urlpatterns():
    """
    Регистрация конфигурации урлов для приложения
    """
    return [url(*controller.urlpattern)]


def register_actions():
    controller.packs.extend([
        actions.UserPack(),
        actions.GroupPack(),
        actions.PermissionPack(),
        actions.ContentTypePack(),
    ])


def register_desktop_menu():
    """
    регистрация элеметов рабочего стола
    """
    desktop.uificate_the_controller(
        controller,
        menu_root=desktop.MainMenu.SubMenu('Demo')
    )
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from objectpack.actions import ObjectPack
from objectpack.ui import ModelEditWindow

from app.controller import observer


class UserPack(ObjectPack):
    url = '/user'

    model = User

    add_to_desktop = True
    add_to_menu = True

    edit_window = add_window = ModelEditWindow.fabricate(
        model,
        model_register=observer,
    )


class GroupPack(ObjectPack):
    url = '/group'

    model = Group

    add_to_desktop = True
    add_to_menu = True

    edit_window = add_window = ModelEditWindow.fabricate(
        model,
        model_register=observer,
    )


class PermissionPack(ObjectPack):
    url = '/Permission'

    model = Permission

    add_to_desktop = True
    add_to_menu = True

    edit_window = add_window = ModelEditWindow.fabricate(
        model,
        model_register=observer,
    )


class ContentTypePack(ObjectPack):
    url = '/ContentType'

    model = ContentType

    add_to_desktop = True
    add_to_menu = True

    edit_window = add_window = ModelEditWindow.fabricate(
        model,
        model_register=observer,
    )